﻿using System;

namespace parcial_2
{
class Empleados
    {
        string[] empleados;
        float[] salarios;

        string[] datos;

        public void Cargar()
        {
            empleados = new String[5];
            salarios = new float[5];
            string[] datos = {"primer","segundo","tercer","cuarto","quinto"};
        


            Console.WriteLine("Datos Empleados");

            for (int i = 0; i < empleados.Length; i++)
            {
                Console.Write("\nEscriba el nombre del {0} empleado: ", datos[i]);
                empleados[i] = Console.ReadLine();
                Console.Write("Escriba el salario del {0} empleado : ", datos[i]);
                salarios[i] = float.Parse(Console.ReadLine());
            }
           Console.Clear();
        }

        public void SalarioMayor()
        {
            float mayor = salarios[0];
            int pos = 0;

            for (int a = 0; a < salarios.Length; a++)
            {
                if (salarios[a] > mayor)
                {
                    mayor = salarios[a];
                    pos = a;
                   
                }
            }
            
            Console.WriteLine("Resultados");
            Console.WriteLine("El empleado con salario mayor es {0}", empleados[pos]);
            Console.WriteLine("Tiene un salario de: "+ mayor );
            
        }

        static void Main(string[] args)
        {
            Empleados Empleado = new Empleados();
            Empleado.Cargar();
            Empleado.SalarioMayor();
        }
    }
}
